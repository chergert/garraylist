all: garraylist

DEBUG = -ggdb
OPTIMIZE = -O0 -fno-omit-frame-pointer
WARNINGS = -Wall -Werror

FILES = \
	garraylist.c \
	garraylist.h \
	arraylist.c

PKGS = glib-2.0

garraylist: $(FILES)
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(FILES) $(shell pkg-config --cflags --libs $(PKGS)) -DGLIB_COMPILATION

clean:
	rm -f garraylist
